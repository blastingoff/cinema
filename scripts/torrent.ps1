Write-Host "Starting..."

# get qBittorrent process
$qbittorrent = Get-Process qbittorrent -ErrorAction SilentlyContinue

# if qBit is running, close it and start timer
if ($qbittorrent) {
  Stop-Process -Name "qbittorrent"
  $x = 3*60 #3 minutes, change this depending on your computer
  $length = $x / 100
  while($x -gt 0) {
    $min = [int](([string]($x/60)).split('.')[0])
    $text = " " + $min + " minutes " + ($x % 60) + " seconds left"
    Write-Progress "Qbit shutting down..." -status $text -perc ($x/$length)
    Start-Sleep -s 1
    $x--
    }
}

Write-Host "Checking for forwarded port."
# Check the forwarded port by PIA
$PortPia = & "C:\Program Files\Private Internet Access\piactl.exe" get portforward
# Convert variable to integer
$PortPia = $PortPia -as [int]

# Replace line in the .ini file
Write-Host "Setting port to" $PortPia"."
$FichierIni = "$env:APPDATA\qBittorrent\qBittorrent.ini"
(Get-Content $FichierIni) -replace "Connection\\PortRangeMin=\d*", "Connection\PortRangeMin=$PortPia" | Set-Content -Path $FichierIni

Start-Sleep -Seconds 5

# start qBit
Start-Process -FilePath "C:\Program Files\qBittorrent\qbittorrent.exe" #remove x86 depending on your install location
Sleep 5

# close the main window of qBit if it's open
$qbittorrent = Get-Process qbittorrent -ErrorAction SilentlyContinue
if ($qbittorrent) {
  $qbittorrent.CloseMainWindow()
} else { #try again in 10 seconds if Qbit hasn't started yet
	Sleep 10
	$qbittorrent = Get-Process qbittorrent -ErrorAction SilentlyContinue
	$qbittorrent.CloseMainWindow()
}

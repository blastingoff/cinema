cd ..

sudo docker compose down
sudo docker images |grep -v REPOSITORY|awk '{print $1}'|xargs -L1 sudo docker pull
sudo docker image prune -a --force --filter "label!=do-not-remove"
sudo docker compose up -d
